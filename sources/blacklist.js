var validator = require('validator');

module.exports = function blacklist() {
    var login = 'login+@';

    var result = validator.blacklist(login, '-+/*@');
    console.log('result: ', result);

    return result;
};