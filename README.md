### Data Structure Sanitizers

In this example we will cover the following functions:

```javascript
    var trimed = validator.trim('input,', ',');
    var escaped = validator.escape('a<b');
    var filtered = validator.blacklist('login+@', '-+/*@');
```

For more information, please refer to the documentation: https://github.com/chriso/validator.js#readme
