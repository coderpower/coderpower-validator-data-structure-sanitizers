var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var blacklist = require('../sources/blacklist');

describe('Object Data Structure : blacklist', function() {

    it('The method blacklist() must be invoked', function() {
        var expected = validator.blacklist('login+@', '-+/*@');
        var result = blacklist();
        expect(result).to.equal(expected);
    });

});