var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var escape = require('../sources/escape');

describe('Object Data Structure : escape', function() {

    it('The method escape() must be invokedd', function() {
        var expected = validator.escape('a<b');
        var result = escape();
        expect(result).to.equal(expected);
    });

});