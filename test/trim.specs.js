var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var trim = require('../sources/trim');

describe('Object Data Structure : trim', function() {

    it('The method trim() must be invoked', function() {
        var expected = validator.trim('input,', ',');
        var result = trim();
        expect(result).to.equal(expected);
    });

});